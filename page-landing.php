<?php
/**
* Template Name: Landing Template
*
*/
$url = get_stylesheet_directory_uri()."/landing/";

$about_us_title = get_field( "about_us_title", false, false );
$about_us_description = get_field( "about_us_description", false, false );
$amount_projects = get_field( "amount_projects", false, false );
$amount_clients = get_field( "amount_clients", false, false );
$amount_industries = get_field( "amount_industries", false, false );
$services_title = get_field( "services_title", false, false );
$services_description = get_field( "services_description", false, false );
$define_title = get_field( "define_title", false, false );
$build_title = get_field( "build_title", false, false );
$scale_title = get_field( "scale_title", false, false );
$define_description = get_field( "define_description", false, false );
$build_description = get_field( "build_description", false, false );
$scale_description = get_field( "scale_description", false, false );

$engagement_type_1_title = get_field( "engagement_type_1_title", false, false );
$engagement_type_1_description = get_field( "engagement_type_1_description", false, false );
$engagement_type_2_title = get_field( "engagement_type_2_title", false, false );
$engagement_type_2_description = get_field( "engagement_type_2_description", false, false );
$engagement_type_3_title = get_field( "engagement_type_3_title", false, false );
$engagement_type_3_description = get_field( "engagement_type_3_description", false, false );

$projects_banner = get_field("projects_banner", false, false);

$contact_us_description = get_field("contact_us_description", false, false);

$meta_partners = get_post_meta('1054', 'partners_gallery', true);
$meta_clients = get_post_meta('1054', 'clients_gallery', true);
$partners_list = "";
$clients_list = "";

if( $images = get_posts( array(
  'post_type' => 'attachment',
  'orderby' => 'post__in', 
  'order' => 'ASC',
  'post__in' => explode(',',$meta_partners), 
  'numberposts' => -1,
  'post_mime_type' => 'image'
) ) ) 
{
  foreach( $images as $image ) {
    $image_src = wp_get_attachment_image_src($image->ID, 'full');
    $width = $image_src[1];
    $height = $image_src[2];
    $partners_list .= '<img width="'.$width.'" height="'.$height.'" src="'.$image_src[0].'" alt="">';
  }
}

if( $images = get_posts( array(
  'post_type' => 'attachment',
  'orderby' => 'post__in', 
  'order' => 'ASC',
  'post__in' => explode(',',$meta_clients), 
  'numberposts' => -1,
  'post_mime_type' => 'image'
) ) ) 
{
  foreach( $images as $image ) {
    $image_src = wp_get_attachment_image_src($image->ID, 'full');
    $width = $image_src[1];
    $height = $image_src[2];
    $clients_list .= '<img width="'.$width.'" height="'.$height.'" src="'.$image_src[0].'" alt="">';
  }
}

$testimonials_html = "";
$testimonial_items = "";
$testimonials = get_posts( 
  array(
    'post_type' => 'testimonials',
    'orderby' => 'post__in', 
    'order' => 'ASC',
    'numberposts' => -1
  ) 
);

$x = 0;
$first_testimonial = "";
foreach( $testimonials as $testimonial ) {
  $id = $testimonial->ID;
  $name = $testimonial->post_title;
  $description = $testimonial->post_content;
  $image_url = get_the_post_thumbnail_url($id, 'full');
  $testimonial = get_field("description", $id);
  $testimonials_html .= '<p class="trust-text">'.$testimonial."<br><br><strong>".$description.'</strong></p>';
  $testimonial_items .= "<li data-position='".$description."' >".$testimonial."</li>";
  if($x==0){
    $first_testimonial = $testimonial."<br><strong>".$description."</strong>";
  }
  $x++;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Tekton</title>
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css"/>
  <link rel="stylesheet" href="<?php echo $url; ?>assets/css/style.css">
</head>
<body>
    <div id="top"></div>
    <header>
      <div class="container-header">
          <div class="logo">
            <a href="#top"><img src="<?php echo $url; ?>assets/images/tk-logo.png" alt=""></a>
          </div>
          <nav>
            <ul>
              <li><a href="#about-us">About us</a></li>
              <li><a href="#services">Services</a></li>
              <li><a href="#trust">Why Tekton</a></li>
            </ul>
          </nav>
          <a href="#contact-us" class="btn blue">Contact us</a>
      </div>
    </header>
    <!--main-->
    <section class="hero">
        <div class="hero-text">
          <p>Creativity</p>
          <p>powers technology</p>
          <a class="btn blue" href="#contact-us">Let's talk</a>
        </div>
      </section>
      
      <section class="about-us" id="about-us">
        <div class="wrapper abus">
          <div class="about-img">
            <img class="img-responsive" src="<?php echo $url; ?>assets/images/about-us.jpg" alt="">
          </div>
          <div class="about-description">
            <p class="section-title"><span class="section-title-blue">About us</span></p>
            <p class="section-title">
              <?php echo($about_us_title); ?>
            </p>
            <p>
              <?php echo($about_us_description); ?>
            </p>
            <a class="btn blue" href="#contact-us">Let's talk</a>
          </div>
        </div>
        <div class="wrapper abus2">
          <div class="about-projects">
            <img src="<?php echo $url; ?>assets/images/icon-project.png" alt="">
            <span class="counter" data-target="<?php echo($amount_projects); ?>">0</span>
            <p class="detail-title">Projects</p>
          </div>
          <div class="about-clients">
            <img src="<?php echo $url; ?>assets/images/icon-clients.png" alt="">
            <span class="counter" data-target="<?php echo($amount_clients); ?>">0</span>
            <p class="detail-title">Clients</p>
          </div>
          <div class="about-industries">
            <img src="<?php echo $url; ?>assets/images/icon-industries.png" alt="">
            <span class="counter" data-target="<?php echo($amount_industries); ?>">0</span>
            <p class="detail-title">Industries</p>
          </div>
        </div>
      </section>

      <section class="services" id="services">
        <div class="wrapper servs">
          <div class="services-title">
            <p class="section-title"><span class="section-title-blue">Services</span></p>
            <p class="section-title">
              <?php echo($services_title); ?>
            </p>
            <p>
              <?php echo($services_description); ?>
            </p>
          </div>
          <div class="services-description">
            <img class="img-responsive" src="<?php echo $url; ?>assets/images/services.jpg" alt="">
          </div>
        </div>
        <div class="wrapper services-cards">
          
          <div class="card">
            <div class="card__inner" id="define-inner">
              <div class="card__face card__face--front define-front">
                <div class="define">
                  <div class="define-svg">
                    <svg width="82" height="82" viewBox="0 0 82 82" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <g clip-path="url(#clip0_48_1202)">
                      <g filter="url(#filter0_i_48_1202)">
                      <path fill-rule="evenodd" clip-rule="evenodd" d="M13.6185 -35C-12.1531 -35 -32.6729 -14.4247 -32.6729 10.5C-32.6729 35.4247 -12.1531 56 13.6185 56C39.3901 56 59.9097 35.4247 59.9097 10.5C59.9097 -14.4247 39.3901 -35 13.6185 -35ZM-50.6729 10.5C-50.6729 -24.5649 -21.8938 -53 13.6185 -53C49.1309 -53 77.9097 -24.5649 77.9097 10.5C77.9097 45.5649 49.1309 74 13.6185 74C-21.8938 74 -50.6729 45.5649 -50.6729 10.5Z" fill="#9C27B0"/>
                      </g>
                      </g>
                      <defs>
                      <filter id="filter0_i_48_1202" x="-50.6729" y="-53" width="128.583" height="131" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                      <feFlood flood-opacity="0" result="BackgroundImageFix"/>
                      <feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape"/>
                      <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
                      <feOffset dy="4"/>
                      <feGaussianBlur stdDeviation="2"/>
                      <feComposite in2="hardAlpha" operator="arithmetic" k2="-1" k3="1"/>
                      <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.02 0"/>
                      <feBlend mode="normal" in2="shape" result="effect1_innerShadow_48_1202"/>
                      </filter>
                      <clipPath id="clip0_48_1202">
                      <rect width="82" height="82" fill="white"/>
                      </clipPath>
                      </defs>
                    </svg>
                  </div>
                  <div class="define-title">
                    <img src="<?php echo $url; ?>assets/images/define-icon.png" alt="">
                    <div><span class="tekton-purple"><?php echo($define_title); ?></span></div>
                  </div>
                  <div class="define-description">
                    <?php echo($define_description); ?>
                  </div>
                  <div class="front-explore-define">
                    <a href="#contact-us">Explore more</a>
                  </div>
                </div>
              </div>
              <div class="card__face card__face--back define-back">
                <div class="card__content">
                  <div class="card__header">
                    <h2><?php echo($define_title); ?></h2>
                  </div>
                  <div class="card__body">
                    <ul>
                      <li>Trends & Insights</li>
                      <li>Service Design</li>
                      <li>Product Discovery</li>
                    </ul>
                  </div>
                  <div class="card__footer">
                    <a href="#contact-us">Explore more</a>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="card">
            <div class="card__inner" id="build-inner">
              <div class="card__face card__face--front build-front">
                <div class="build">
                  <div class="build-svg">
                    <svg width="110" height="148" viewBox="0 0 110 148" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <g clip-path="url(#clip0_50_1233)">
                      <g filter="url(#filter0_i_50_1233)">
                      <path fill-rule="evenodd" clip-rule="evenodd" d="M115.648 125C141.587 125 162.137 104.368 162.137 79.5C162.137 54.6323 141.587 34 115.648 34C89.71 34 69.1589 54.6323 69.1589 79.5C69.1589 104.368 89.71 125 115.648 125ZM115.648 143C151.264 143 180.137 114.57 180.137 79.5C180.137 44.4299 151.264 16 115.648 16C80.0317 16 51.1589 44.4299 51.1589 79.5C51.1589 114.57 80.0317 143 115.648 143Z" fill="#478DFF"/>
                      </g>
                      </g>
                      <defs>
                      <filter id="filter0_i_50_1233" x="51.1589" y="16" width="128.978" height="131" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                      <feFlood flood-opacity="0" result="BackgroundImageFix"/>
                      <feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape"/>
                      <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
                      <feOffset dy="4"/>
                      <feGaussianBlur stdDeviation="2"/>
                      <feComposite in2="hardAlpha" operator="arithmetic" k2="-1" k3="1"/>
                      <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.02 0"/>
                      <feBlend mode="normal" in2="shape" result="effect1_innerShadow_50_1233"/>
                      </filter>
                      <clipPath id="clip0_50_1233">
                      <rect width="110" height="148" fill="white"/>
                      </clipPath>
                      </defs>
                    </svg>
                  </div>
                  <div class="build-title">
                    <img src="<?php echo $url; ?>assets/images/build-icon.png" alt="">
                    <div><span class="tekton-blue"><?php echo($build_title); ?></span></div>
                  </div>
                  <div class="build-description">
                    <?php echo($build_description); ?>
                  </div>
                  <div class="front-explore-build">
                    <a href="#contact-us">Explore more</a>
                  </div>
                </div>
              </div>
              <div class="card__face card__face--back build-back">
                <div class="card__content">
                  <div class="card__header">
                    <h2><?php echo($build_title); ?></h2>
                  </div>
                  <div class="card__body">
                    <ul>
                      <li>Experience Design</li>
                      <li>UX/UI</li>
                      <li>Product Engineering</li>
                      <li>Web & Mobile Development</li>
                      <li>Quality Assurance</li>
                      <li>LifeCycle Management</li>
                    </ul>
                  </div>
                  <div class="card__footer">
                    <a href="#contact-us">Explore more</a>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="card">
            <div class="card__inner" id="scale-inner">
              <div class="card__face card__face--front scale-front">
                <div class="scale">
                  <div class="scale-svg">
                    <svg width="153" height="65" viewBox="0 0 153 65" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <g clip-path="url(#clip0_53_1261)">
                      <path fill-rule="evenodd" clip-rule="evenodd" d="M68.4 45C94.177 45 114.692 24.4194 114.692 -0.5C114.692 -25.4194 94.177 -46 68.4 -46C42.624 -46 22.109 -25.4194 22.109 -0.5C22.109 24.4194 42.624 45 68.4 45ZM68.4 63C103.907 63 132.692 34.5701 132.692 -0.5C132.692 -35.5701 103.907 -64 68.4 -64C32.893 -64 4.10913 -35.5701 4.10913 -0.5C4.10913 34.5701 32.893 63 68.4 63Z" fill="#034752"/>
                      </g>
                      <defs>
                      <clipPath id="clip0_53_1261">
                      <rect width="153" height="65" fill="white"/>
                      </clipPath>
                      </defs>
                    </svg>
                  </div>
                  <div class="scale-title">
                    <img src="<?php echo $url; ?>assets/images/scale-icon.png" alt="">
                    <div><span class="tekton-green"><?php echo($scale_title); ?></span></div>
                  </div>
                  <div class="scale-description">
                    <?php echo($scale_description); ?>
                  </div>
                  <div class="front-explore-scale">
                    <a href="#contact-us">Explore more</a>
                  </div>
                </div>
              </div>
              <div class="card__face card__face--back scale-back">
                <div class="card__content">
                  <div class="card__header">
                    <h2><?php echo($scale_title); ?></h2>
                  </div>
                  <div class="card__body">
                    <ul>
                      <li>DevOps</li>
                      <li>Cloud Enablement</li>
                      <li>Data & Machine Learning</li>
                    </ul>
                  </div>
                  <div class="card__footer">
                    <a href="#contact-us">Explore more</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <div class="wrapper engagement">
          <p class="section-title-blue">Our</p>
          <p class="section-title">
            <strong>Types of engagement</strong>
          </p>
          <div class="engagement-types">
            <div class="engagement-type engagement-teams">
              <div class="engagement-type-icon">
                <img src="<?php echo $url; ?>assets/images/icon-teams.png" width="32" height="32" alt="">
              </div>
              <div class="engagement-title">
                <?php echo($engagement_type_1_title); ?>
              </div>
              <div class="engagement-description">                
                <?php echo($engagement_type_1_description); ?>
              </div>
            </div>
            <div class="engagement-type engagement-staff">
              <div class="engagement-type-icon">
                <img src="<?php echo $url; ?>assets/images/icon-staff.png" width="32" height="32" alt="">
              </div>
              <div class="engagement-title">
                <?php echo($engagement_type_2_title); ?>
              </div>
              <div class="engagement-description">                
                <?php echo($engagement_type_2_description); ?>
              </div>
            </div>
            <div class="engagement-type engagement-projects">
              <div class="engagement-type-icon">
                <img src="<?php echo $url; ?>assets/images/icon-projects.png" width="32" height="32" alt="">
              </div>
              <div class="engagement-title">
                <?php echo($engagement_type_3_title); ?>
              </div>
              <div class="engagement-description">                
                <?php echo($engagement_type_3_description); ?>
              </div>  
            </div>
          </div>
        </div>
      </section>

      <section class="hero-improve">
        <div class="wrapper" id="improve">
          <div>
            <p>We exist to<br>
              <span>
                improve
                people's lives</span>
            </p>
          </div>
          <div class="improve-description-container">
            <div class="improve-description">
              <?php echo($projects_banner); ?>
            </div>
          </div>
        </div>
      </section>

      <section class="trust" id="trust">
        <div class="wrapper">
          <p class="section-title">Who <strong>trust us:</strong></p>
          <div class="companies">
            <?php echo($clients_list); ?>
          </div>
          <!--p class="trust-text">
            <?php //echo($first_testimonial); ?>
          </p-->
          <!--ul id="testimonials" style="display: none;">
            <?php //echo($testimonial_items); ?>
          </ul-->
          <div class="clients">
            <?php echo($testimonials_html); ?>
          </div>
        </div>
      </section>

      <section class="partners">
        <div class="wrapper">
          <div class="partners-list">
            <?php echo($partners_list); ?>
          </div>
        </div>
      </section>

      <section class="contact-us" id="contact-us">
        <div class="wrapper">
          <div class="contact-form">
            <div class="contact-info">
              <p class="section-title"><strong>Contact us</strong></p>
              <p>
                <?php echo($contact_us_description); ?>
              </p>
            </div>
            <div class="form">
              <script charset="utf-8" type="text/javascript" src="https://js.hsforms.net/forms/v2.js"></script>
              <script>
                hbspt.forms.create({
                  region: "na1",
                  portalId: "20966237",
                  formId: "e8892038-6806-42c3-913d-a9cdd2012626"
                });
              </script>
            </div>
          </div>
        </div>
      </section>
    <!--/main-->
    
    <footer>
      <div class="container-footer">
        <div class="logo">
          <img src="<?php echo $url; ?>assets/images/tk-logo.png" alt="">
          <p>
            <ul class="social-networks">
              <li><a href="https://www.linkedin.com/company/tekton-labs/"><img src="<?php echo $url; ?>assets/images/LinkedIn.png" alt=""></a></li>
              <li><a href="https://www.facebook.com/tektonlabs"><img src="<?php echo $url; ?>assets/images/Facebook.png" alt=""></a></li>
              <li><a href="https://www.instagram.com/tektonlabs/"><img src="<?php echo $url; ?>assets/images/Instagram.png" alt=""></a></li>
            </ul>
            <div class="mail-footer">
              <i class="icon-mail"></i><a href="mailto:hi@tekton.com">hi@tekton.com</a>
            </div>
          </p>
        </div>
        <ul class="footer-menu">
          <li><a href="#about-us">About us</a></li>
          <li><a href="#services">Services</a></li>
          <li><a href="#trust">Why Tekton</a></li>
        </ul>
        <div>
          <p><strong>Our offices:</strong></p>
            <ul>
              <li>
                <strong>Lima</strong><br>
                Av. Jose Pardo Nro. 434, piso 16, Miraflores <br>
                <a href="tel:+51977492633">+51 977 492 633</a>
              </li>
              <li>
                <strong>Mexico City (DF)</strong><br>
                Prol. Paseo de la Reforma 1236 Piso 5, Santa Fe CP, 05348 <br>
                <a href="tel:+5241648528">+52 4164 8528</a>
              </li>
            </ul>
          
        </div>
        <div>
          <ul>
            <li>
              <strong>Silicon Valley</strong><br>
              800 W. El Camino Real, Suite 180, Mountain View, CA 94040 <br>
              <a href="tel:+16502674703">+1 650 267 4703</a>
            </li>
            <li>
              <strong>Charlotte</strong><br>
              28 South Tryon St, Charlotte, NC 28202 <br>
              <a href="tel:+16502674703">+1 650 267 4703</a>
            </li>
          </ul>
        </div>
      </div>
    </footer>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script src="<?php echo $url; ?>assets/js/app.js"></script>
</body>
</html>