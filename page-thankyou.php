<?php
/**
* Template Name: Thank You Landing Template
*
*/
$url = get_stylesheet_directory_uri()."/landing/";
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Tekton</title>
  <link rel="stylesheet" href="<?php echo $url; ?>assets/css/style.css">
</head>
<body>
    <a name="top"></a>
    <header>
      <div class="container-header">
          <div class="logo">
            <a href="#top"><img src="<?php echo $url; ?>assets/images/tk-logo.png" alt=""></a>
          </div>
          <nav>
            <ul>
              <li><a href="#about-us">About us</a></li>
              <li><a href="#services">Services</a></li>
              <li><a href="#trust">Why Tekton</a></li>
            </ul>
          </nav>
          <a href="#contact-us" class="btn blue">Contact us</a>
      </div>
    </header>
    <!--main-->
    <section class="hero-thanks">
      <div class="wrapper" id="thank-you">
        <div class="thankyou-description-container">
          <div class="thankyou-description">
            <p>BRING IT ON</p>
            <p>
              Thank you for contacting Tekton.
            </p>
            <p>
              Someone from our team will be following
up with you any minute.
            </p>
            <p>
              <a href="https://www.tektonlabs.com/" class="btn blue">Go visit our website</a>
            </p>
          </div>
        </div>
        <div>
          
        </div>
      </div>
    </section>
    <!--/main-->
    <footer>
      <div class="container-footer">
        <div class="logo">
          <img src="<?php echo $url; ?>assets/images/tk-logo.png" alt="">
          <p>
            <ul class="social-networks">
              <li><a href="https://www.linkedin.com/company/tekton-labs/"><img src="<?php echo $url; ?>assets/images/LinkedIn.png" alt=""></a></li>
              <li><a href="https://www.facebook.com/tektonlabs"><img src="<?php echo $url; ?>assets/images/Facebook.png" alt=""></a></li>
              <li><a href="https://www.instagram.com/tektonlabs/"><img src="<?php echo $url; ?>assets/images/Instagram.png" alt=""></a></li>
            </ul>
            <div class="mail-footer">
              <i class="icon-mail"></i><a href="mailto:hi@tekton.com">hi@tekton.com</a>
            </div>
          </p>
        </div>
        <ul class="footer-menu">
          <li><a href="#">About us</a></li>
          <li><a href="#">Services</a></li>
          <li><a href="#">Why Tekton</a></li>
        </ul>
        <div>
          <p><strong>Our offices:</strong></p>
            <ul>
              <li>
                <strong>Lima</strong><br>
                Av. Jose Pardo Nro. 434, piso 16, Miraflores <br>
                <a href="tel:+51977492633">+51 977 492 633</a>
              </li>
              <li>
                <strong>Mexico City (DF)</strong><br>
                Prol. Paseo de la Reforma 1236 Piso 5, Santa Fe CP, 05348 <br>
                <a href="tel:+5241648528">+52 4164 8528</a>
              </li>
            </ul>
          
        </div>
        <div>
          <ul>
            <li>
              <strong>Silicon Valley</strong><br>
              800 W. El Camino Real, Suite 180, Mountain View, CA 94040 <br>
              <a href="tel:+16502674703">+1 650 267 4703</a>
            </li>
            <li>
              <strong>Charlotte</strong><br>
              28 South Tryon St, Charlotte, NC 28202 <br>
              <a href="tel:+16502674703">+1 650 267 4703</a>
            </li>
          </ul>
        </div>
      </div>
    </footer>
</body>
</html>