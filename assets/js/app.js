let section_counter = document.querySelector('.abus2');
let counters = document.querySelectorAll('.counter');

let CounterObserver = new IntersectionObserver(
  (entries, observer) => {
    let [entry] = entries;
    if (!entry.isIntersecting) return;

    let speed = 200;
    counters.forEach((counter, index) => {
      function UpdateCounter() {
        const targetNumber = +counter.dataset.target;
        const initialNumber = +counter.innerText;
        const incPerCount = targetNumber / speed;
        if (initialNumber < targetNumber) {
          counter.innerText = Math.ceil(initialNumber + incPerCount);
          setTimeout(UpdateCounter, 6);
        }
      }
      UpdateCounter();

      if (counter.parentElement.style.animation) {
        counter.parentElement.style.animation = '';
      } else {
        counter.parentElement.style.animation = `slide-up 0.3s ease forwards ${
          index / counters.length + 0.5
        }s`;
      }
    });
    observer.unobserve(section_counter);
  },
  {
    root: null,
    threshold: window.innerWidth > 768 ? 0.4 : 0.3,
  }
);

CounterObserver.observe(section_counter);


const card_define = document.querySelector("#define-inner");
const card_build = document.querySelector("#build-inner");
const card_scale = document.querySelector("#scale-inner");

jQuery(document).on('click', 'a[href^="#"]', function (event) {
  event.preventDefault();
  var innerWidth = window.innerWidth;
  console.log(innerWidth);

  switch(jQuery.attr(this, 'href')){
    case "#about-us":
      if(innerWidth <= 850){
        jQuery('html, body').animate({
          scrollTop: jQuery(jQuery.attr(this, 'href')).offset().top - 80
        }, 500);
        break;
      }else{
        jQuery('html, body').animate({
          scrollTop: jQuery(jQuery.attr(this, 'href')).offset().top
        }, 500);
        break;
      }

    case "#services":
        if(innerWidth <= 850){
          jQuery('html, body').animate({
            scrollTop: jQuery(jQuery.attr(this, 'href')).offset().top - 100
          }, 500);
          break;
        }else{
          jQuery('html, body').animate({
            scrollTop: jQuery(jQuery.attr(this, 'href')).offset().top
          }, 500);
          break;
        }
        
    case "#trust":
      jQuery('html, body').animate({
        scrollTop: jQuery(jQuery.attr(this, 'href')).offset().top - 160
      }, 500);
      break;

    default:
      jQuery('html, body').animate({
        scrollTop: jQuery(jQuery.attr(this, 'href')).offset().top - 80
      }, 500);
      break;
  }

});

card_define.addEventListener("mouseover", function (e) {
  jQuery(".define-front").addClass("hide_div");
  jQuery(".define-back").addClass("show_div");
});

card_define.addEventListener("mouseout", function (e) {
  jQuery(".define-front").removeClass("hide_div");
  jQuery(".define-front").addClass("show_div");
  jQuery(".define-back").removeClass("show_div");
  jQuery(".define-back").addClass("hide_div");
});

card_build.addEventListener("mouseover", function (e) {
  jQuery(".build-front").addClass("hide_div");
  jQuery(".build-back").addClass("show_div");
});

card_build.addEventListener("mouseout", function (e) {
  jQuery(".build-front").removeClass("hide_div");
  jQuery(".build-front").addClass("show_div");
  jQuery(".build-back").removeClass("show_div");
  jQuery(".build-back").addClass("hide_div");
});

card_scale.addEventListener("mouseover", function (e) {
  jQuery(".scale-front").addClass("hide_div");
  jQuery(".scale-back").addClass("show_div");
});

card_scale.addEventListener("mouseout", function (e) {
  jQuery(".scale-front").removeClass("hide_div");
  jQuery(".scale-front").addClass("show_div");
  jQuery(".scale-back").removeClass("show_div");
  jQuery(".scale-back").addClass("hide_div");
});

jQuery(document).ready(function(){
  jQuery('.clients').slick({
    autoplay: true,
    slidesToShow: 1,
    arrows: false, 
    dots: true,
    fade:false,
    responsive: [
      {
        breakpoint: 900,
        settings: {
          arrows: false,
          centerPadding: '0px',
        }
      },
      {
        breakpoint: 500,
        settings: {
          arrows: false,
          centerPadding: '0px',
        }
      }
    ]
  });
});